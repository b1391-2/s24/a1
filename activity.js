// Activity

 // 1 Find users with letter S in their first name or d in their last name


 db.users.find(
 	{
 		$or : [
 			{ firstName : {$regex : 'S'}},
 			{ lastName : {$regex : 'D'}}
 		]
 	},
 	{
 		firstName : 1,
 		lastName : 1,
 		_id: 0
 	}
 );

 //2 Find users who are in HR Department and their age is greater than or equal to 70 using and operator

 db.users.find(
 	{
 		$and : [
 			{ department : "HR"},
 			{ age : { $gte : 70}}
 		]
 	}
 );
 

 // 3 Find users with the letter e in their first name  and has an age of less than or equal to 30 using $and, $regex, $lte ops

 db.users.find(
 	{
 		$and : [
 			{ firstName : {$regex : 'E', $options : '$i'}},
 			{ age : { $lte : 30}}
 		]
 	}
 );